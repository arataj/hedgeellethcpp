/*
 * ConstructorDelegationWorkaround.java
 *
 * Created on Jul 15, 2011, 1:07:30 PM
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */
package pl.gliwice.iitis.hedgeelleth.compiler.backend.cpp;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Optimizer;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.MethodSignature;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A workaround for missing constructor delegation in C++ before
 * C++0x.
 * 
 * @author Artur Rataj
 */
public class ConstructorDelegationWorkaround {
    /**
     * A prefix added to a constructor's name, to create the name
     * of the equivalent method.
     */
    public static final String EQUIVALENT_METHOD_PREFIX =
            "EQ" + CPPBackend.SPECIAL_SEPARATOR;
    /**
     * An error message, if super() was traced to contain
     * a non--constant and a non--argument, what is not
     * supported by the C++ backend.
     */
    public static final String BAD_ARGUMENT_MESSAGE = "C++ backend " +
            "requires caller arguments or constants in superconstructor calls";
       
    /**
     * Backend, that uses this class.
     */
    CPPBackend backend;
    /**
     * Equivalent methods, to be added to the currently processed
     * class. Key is a signature name.
     */
    public Map<String, CodeMethod> pendingEquivalents;
    
    /**
     * Creates a new instance of ConstructorDelegationWorkaround.
     * One such object is used for a whole compilation.
     * 
     * @param backend backend, that uses this class.
     */
    public ConstructorDelegationWorkaround(CPPBackend backend) {
        this.backend = backend;
        pendingEquivalents = new TreeMap<String, CodeMethod>();
    }
    /**
     * Searches for a given value in a caller constructor, given
     * a call from that constructor.<br>
     * 
     * This method is used to trace arguments of super() to
     * the root constructor in the constructor call
     * chain.
     * 
     * @param r a value to search
     * @param c a caller method with the call, the caller must
     * be a constructor
     * @param rootCaller if an argument should be returned
     * instead of its index
     * @return a constant if the call contained it, or an
     * index of caller method's argument - 1 if <code>rootCaller</code>
     * is false, or an argument variable of the caller method if
     * <code>rootCaller</code> is true, or a position in the
     * stream if a call contains an invalid argument; in the
     * last case, an error should be thrown containing the
     * message in <code>BAD_ARGUMENT_MESSAGE</code>
     */
    public static Object getCallerArgument(RangedCodeValue r, ConstructorCaller c,
            boolean rootCaller) {
        Object o = null;
        if(r.range != null)
            o = c.call.getStreamPos();
        else if(r.value instanceof CodeConstant) {
            o = r.value;
        } else {
            if(!(r.value instanceof CodeFieldDereference))
                o = c.call.getStreamPos();
            else {
                CodeFieldDereference f = (CodeFieldDereference)r.value;
                Collection<CodeVariable> l = f.extractPlainLocal();
                if(l.isEmpty())
                    o = c.call.getStreamPos();
                else {
                    CodeVariable local = l.iterator().next();
                    boolean isArgument = false;
                    int argCount = 0;
                    for(CodeVariable arg : c.method.args) {
                        if(arg == local) {
                            if(rootCaller)
                                // a root call, take the variable
                                o = arg;
                            else
                                // a sub--call, take an index 
                                o = argCount - 1;
                            isArgument = true;
                            break;
                        }
                        ++argCount;
                    }
                    if(!isArgument)
                        o = c.call.getStreamPos();
                }
            }
        }
        return o;
    }
    /**
     * Returns a constant textual representation or
     * a variable name, depending on the argument's type.
     * 
     * @param o a constant or a variable
     * @return a text representing the argument
     */
    public static String constantOrVariableToString(Object o) {
        if(o instanceof CodeConstant) {
            CodeConstant c = (CodeConstant)o;
            return c.value.toJavaString();
        } else if(o instanceof CodeVariable) {
            CodeVariable cv = (CodeVariable)o;
            return cv.name; 
        } else
            throw new RuntimeException("constant or variable expected");
    }
    /**
     * Processes a list of callers to find the names of the
     * arguments of the final <code>super()</code>.
     * 
     * @param callers a list of callers, from the root
     * constructor to the one calling a superconstructor
     * @param translationErrors if there are any translation
     * errors, then they are added to this exception 
     * @return a string with arguments of <code>super()</code>
     */
    String processCallers(List<ConstructorCaller> callers, CompilerException translationErrors) {
        String out = "";
        // a code constant or an integer, that is an argument's index,
        // or a stream position of an invalid call
        Object[] arguments = null;
        int callerCount = 0;
        for(ConstructorCaller c : callers) {
            if(arguments == null)
                arguments = new Object[c.call.args.size() - 1];
            int count = 0;
            Object[] callerArguments = new Object[c.call.args.size() - 1];
            for(RangedCodeValue r : c.call.args) {
                // skip "this"
                if(count != 0)
                    callerArguments[count - 1] = getCallerArgument(r, c,
                            callerCount == callers.size() - 1);
                ++count;
            }
            for(int i = 0; i < arguments.length; ++i) {
                Object p = arguments[i];
                if(p == null) {
                    arguments[i] = callerArguments[i];
                } else {
                    if(p instanceof Integer) {
                        int index = (Integer)p;
                        arguments[i] = callerArguments[index];
                    } else if(!(p instanceof CodeConstant))
                        throw new RuntimeException("a constant expected");
                }
                if(arguments[i] instanceof StreamPos) {
                    translationErrors.addReport(new CompilerException.Report(
                            (StreamPos)arguments[i],
                            CompilerException.Code.ILLEGAL, BAD_ARGUMENT_MESSAGE));
                    // an arbitrary constant, the compiler will report
                    // an error anyway
                    arguments[i] = new CodeConstant(null, new Literal(-1));
                }
            }
            ++callerCount;
        }
        for(int i = 0; i < arguments.length; ++i) {
            Object o = arguments[i];
            out += constantOrVariableToString(o);
            if(i != arguments.length - 1)
                out += ", ";
        }
        return out;
    }
    /**
     * Recursively searches through called "this" constructors,
     * until one calling a superconstructor is found. 
     * Respective methods with the equivalent of the constructor's
     * code, if not already exist, are created.<br>
     * 
     * The called of this method must translate <code>call</code>
     * in the generated output
     * to code that calls the equivalent method, and attach the
     * return value to the method it generates. This way, the
     * chain of calls is attached at both ends.
     * 
     * @param call a call to "this" constructor
     * @param method a method, to which <code>call</code> belongs
     * @return a super constructor, called at the end of the
     * "this" constructor calling chain
     */
    public String getFinalSuperCalled(CodeOpCall call, CodeMethod method)
            throws CompilerException {
        String out = null;
        CodeMethod thiz = call.methodReference.method;
        CodeClass thisClass = thiz.owner;
        CodeMethod curr = thiz;
        Stack<CodeOpCall> callStack = new Stack<CodeOpCall>();
        Stack<CodeMethod> methodStackAll = new Stack<CodeMethod>();
        Stack<CodeMethod> methodStackNotCorrected = new Stack<CodeMethod>();
        while(curr != null) {
            if(curr.code.ops.isEmpty())
                // Object is a non--compilable class, thus, should not be
                // translated by this backend
                throw new RuntimeException("a non--Object constructor " +
                        "is empty");
            AbstractCodeOp op = null;
            int opIndex = -1;
            String thisName = thisClass.getUnqualifiedName();
            String superName = thisClass.extendS.getUnqualifiedName();
            int count = 0;
            for(AbstractCodeOp o : curr.code.ops) {
                if(o instanceof CodeOpCall) {
                    CodeOpCall c = (CodeOpCall)o;
                    String n = c.methodReference.method.signature.extractQualifiedNameString();
                    if(n.equals(thisName) || n.equals(superName)) {
                        op = c;
                        opIndex = count;
                        break;
                    }
                }
                ++count;
            }
            if(op == null)
                // Object is a non--compilable class, thus, should not be
                // translated by this backend
                throw new RuntimeException("a non--Object constructor " +
                        "does not start with a call");
            String equivalentSignature = EQUIVALENT_METHOD_PREFIX +
                    curr.signature.name;
            CodeMethod currEq;
            if(pendingEquivalents.containsKey(equivalentSignature))
                currEq = null;
            else {
                // a new equivalent method needs to be created
                currEq = new CodeMethod(curr);
                currEq.signature = new MethodSignature(null,
                        equivalentSignature);
                currEq.apI = false;
                pendingEquivalents.put(equivalentSignature,
                    currEq);
            }
            CodeOpCall currCall = (CodeOpCall)op;
            CodeMethod next = currCall.methodReference.method;
            // next call to "this" or "super" found
            currCall = (CodeOpCall)currCall.clone();
            callStack.add(currCall);
            if(currEq != null) {
                // prepare this method for further correction
                currEq.code.ops.set(opIndex, currCall);
                methodStackAll.add(currEq);
            } else {
                methodStackAll.add(pendingEquivalents.get(
                        equivalentSignature));
            }
            methodStackNotCorrected.add(currEq);
            curr = next;
            if(next.owner != thisClass) {
                // a call to "super" found"
                //
                // create the superconstructor string
                CodeClass superClass = next.owner;
                if(superClass != thisClass.extendS)
                    throw new RuntimeException("a constructor " +
                            "calls a non--this and a non--super class");
                out = backend.getClass(superClass) + "(";
                CodeMethod prev = null;
                List<ConstructorCaller> callers = new LinkedList<ConstructorCaller>();
                // correct the invocations in the equivalent
                // methods
                while(!callStack.isEmpty()) {
                    currCall = callStack.pop();
                    currEq = methodStackNotCorrected.pop();
                    if(currEq != null) {
                        if(prev == null) {
                            // this is replaced by <code>out</code>
                            currEq.code.ops.remove(opIndex);
                            Optimizer.setLabelIndices(currEq);
                        } else {
                            // no longer actual, remove
                            currCall.methodReference.firstPassMethod = null;
                            // replace call to "this" with a call to the
                            // equivalent method
                            currCall.methodReference.method = prev;
                        }
                    }
                    prev = methodStackAll.pop();
                    callers.add(new ConstructorCaller(prev, currCall));
                }
                callers.add(new ConstructorCaller(method, call));
                CompilerException translationErrors = new CompilerException();
                out += processCallers(callers, translationErrors) + ")";
                if(translationErrors.reportsExist())
                    throw translationErrors;
                // the call to the bottommost method in the stack must
                // be corrected by the called of <code>getFinalSuperCalled</code>
                curr = null;
            }
        }
        return out;
    }
}
