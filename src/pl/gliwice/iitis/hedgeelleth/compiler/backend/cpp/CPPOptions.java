/*
 * CPPOptions.java
 *
 * Created on May 5, 2011, 4:02:05 PM
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.cpp;

/**
 * Options for <code>CPPBackend</code>. Initialized with default values.
 * 
 * @author Artur Rataj
 */
public class CPPOptions {
    /**
     * If to add preambles before the header and implementation files,
     * the default is true.<br>
     * 
     * The preamble may vary depending on Hedgeelleth
     * version, thus, it should probably not be used for regression
     * tests with pattern files.
     */
    public boolean usePreamble = true;
    /**
     * If to use the C++0x style constructor delegation. If false, then
     * additional methods are generated, that copy the code of "this"
     * constructors. The default is false for compatibility.
     */
    public boolean constructorDelegation = false;
}
