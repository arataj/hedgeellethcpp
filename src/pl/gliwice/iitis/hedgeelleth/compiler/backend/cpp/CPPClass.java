/*
 * CPPClass.java
 *
 * Created on May 4, 2011, 1:32:28 PM
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.cpp;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeClass;

/**
 * Represents a C++ equivalent of a given <code>CodeClass</code> object.
 * 
 * @author Artur Rataj
 */
public class CPPClass {
    /**
     * Hedgeelleth's code class, represented by this object.
     */
    public CodeClass cc;
    /**
     * Fully qualified name of the <code>CodeClass</code> object,
     * in Hedgeelleth dot--separated form.
     */
    public String fullName;
    /**
     * Class name, without the qualifiers.
     */
    public String name;
    /**
     * Namespace of the C++ class.
     */
    public String namespace;
    
    /**
     * Creates a new instance of CPPClass, on the basic of a given code class.
     * 
     * @param cc Hedgeelleth's code class
     * @param translationErrors exception to add possible parse errors
     */
    public CPPClass(CodeClass cc, CompilerException translationErrors) {
        this.cc = cc;
        fullName = this.cc.name;
        name = this.cc.getUnqualifiedName();
        if(name.indexOf(CPPBackend.SPECIAL_SEPARATOR) != -1)
            translationErrors.addReport(new CompilerException.Report(null,
                    CompilerException.Code.ILLEGAL,
                    "class " + fullName + " contains `" + CPPBackend.SPECIAL_SEPARATOR +
                    "' in its qualified name, " +
                    "what conflicts with the `" + CPPBackend.SPECIAL_SEPARATOR +
                    "' namespace element used by the C++ backend"));
        else {
            namespace = CPPBackend.toNamespace(fullName);
        }
    }
}
