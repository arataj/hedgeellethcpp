/*
 * ConstructorCaller.java
 *
 * Created on Jul 28, 2011, 6:34:45 PM
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */
package pl.gliwice.iitis.hedgeelleth.compiler.backend.cpp;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeMethod;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpCall;

/**
 * A wrapper for a constructor and a call within it.
 */
public class ConstructorCaller {
    /**
     * A method.
     */
    CodeMethod method;
    /**
     * A call operation within the method.
     */
    CodeOpCall call;

    /**
     * Constructs a new instance of <code>Caller</code>.
     * 
     * @param method a method
     * @param call a call operation within the method
     */
    ConstructorCaller(CodeMethod method, CodeOpCall call) {
        this.method = method;
        this.call = call;
    }
};
